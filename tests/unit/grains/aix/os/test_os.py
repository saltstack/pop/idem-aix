from dict_tools import data
import pytest
import mock


@pytest.mark.asyncio
async def test_load_os(mock_hub, hub):
    mock_hub.exec.cmd.run.side_effect = [
        data.NamespaceDict({"stdout": "7.1.0.0"}),
        data.NamespaceDict({"stdout": "AIX"}),
    ]

    with mock.patch("shutil.which", return_value=True):
        mock_hub.grains.aix.os.os.load_os = hub.grains.aix.os.os.load_os
        await mock_hub.grains.aix.os.os.load_os()

    assert mock_hub.grains.GRAINS.osrelease == "7.1.0.0"
    assert mock_hub.grains.GRAINS.osrelease_info == (7, 1, 0, 0)
    assert mock_hub.grains.GRAINS.osmajorrelease == 7
    assert mock_hub.grains.GRAINS.osfullname == "AIX"
    assert mock_hub.grains.GRAINS.osfinger == "AIX-7"
    assert mock_hub.grains.GRAINS.oscodename == "unknown"


@pytest.mark.asyncio
async def test_load_build(mock_hub, hub):
    mock_hub.exec.cmd.run.return_value = data.NamespaceDict({"stdout": "1234455679"})

    with mock.patch("shutil.which", return_value=True):
        mock_hub.grains.aix.os.os.load_build = hub.grains.aix.os.os.load_build
        await mock_hub.grains.aix.os.os.load_build()

    assert mock_hub.grains.GRAINS.osbuild == "1234455679"
